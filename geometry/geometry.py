import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
import math
import random
import numpy as np


plt.axis([-20,20,-20,20])
fig = plt.figure()
ax = fig.add_subplot(111, aspect='equal')
# ax.set_xticklabels([])
# ax.set_yticklabels([])

ax.axis('off')

eColor = '#121212'
Color1 = '#00B2EE'
Color2 = '#FF7D40'
Color3 = '#2ca02c'
Color4 = '#d62728'

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', serif='Helvetica Neue')

rectW = 10*1E-6 # [um]
rectH = 10*1E-6 # [um]
cellR = 3*1E-6 # [um]
plt.xlim(-0.6*rectW, 0.6*rectW)
plt.ylim(-0.6*rectH, 0.6*rectH)

npoint = 60
angles = np.linspace(0, 2*math.pi * (npoint-1.0)/npoint, npoint)
cellPts = [[cellR*math.cos(angle), cellR*math.sin(angle)] for angle in angles]

# draw rectangle
ax.add_patch(patches.Rectangle([-0.5*rectW, -0.5*rectH], rectW, rectH, ec=eColor, fc='None'))

# draw cell
ax.add_patch(patches.Polygon(cellPts, facecolor='none', edgecolor='#FF7D40'))

fig.savefig('./cell-with-membrane.pdf', dpi=90, bbox_inches='tight')

def writeResolution(f):
    f.write("lc = 0.34*1e-6;\n")
    f.write("lc2 = 0.24*1e-6;\n\n")

def writePointsLinesLoop(f, pts, startPointIdx=1, startLineIdx=1, loopId=1, name="POLYGON", lcStr='lc'):
    f.write("\n// " + name + " \n")
    f.write("\n// Points\n\n")

    np = startPointIdx
    for i in xrange(len(pts)):
        pi = pts[i]
        f.write("p{0} = newp;\n".format(np))
        f.write("Point(p{0}) = {{{1}, {2}, {3}, {4}}};\n".format(np, pi[0], pi[1], 0, lcStr))
        np += 1

    nl = startLineIdx
    f.write("\n// Lines\n\n")
    for i in xrange(len(pts)):
        f.write("l{0} = newl;\n".format(nl))
        i1 = startPointIdx+i
        i2 = startPointIdx+i+1 if (i < len(pts)-1) else startPointIdx
        f.write("Line(l{0}) = {{p{1}, p{2}}};\n".format(nl, i1, i2))
        nl += 1

    f.write("\n// Line Loops\n\n")
    nll = loopId
    f.write("ll{0} = newll;\n".format(nll))

    llStr = "{{l{0}".format(startLineIdx)

    for i in xrange(startLineIdx+1, nl):
        llStr += ", l{0}".format(i)

    llStr += "}"
    f.write("Line Loop(ll{0}) = ".format(nll) + llStr + ";\n")

    return np, nl, llStr


# EXPORT
f1 = open("./membrane-potential.geo","w")

# point resolution
writeResolution(f1)

# rectangle
rectPts = [[-0.5*rectW, -0.5*rectH], [0.5*rectW, -0.5*rectH], [0.5*rectW, 0.5*rectH], [-0.5*rectW, 0.5*rectH]]
countPoint, countLine, rectLLStr = writePointsLinesLoop(f1, rectPts, startPointIdx=1, startLineIdx=1, loopId=1, name="RECTANGLE")

# cell
countPoint, countLine, cellLLStr = writePointsLinesLoop(f1, cellPts, startPointIdx=countPoint, startLineIdx=countLine, loopId=2, name="CELL", lcStr='lc2')

f1.write("Physical Line(1) = " + cellLLStr + ";\n\n")

f1.write("surf{0} = news;\n".format(1))
f1.write("Plane Surface(surf{0}) = {{ll{1}, ll{2}}}; \n".format(1, 1, 2))

f1.write("surf{0} = news;\n".format(2))
f1.write("Plane Surface(surf{0}) = {{ll{1}}}; \n".format(2, 2))

f1.write("Physical Surface(0) = {{surf{0}}}; \n".format(1))
f1.write("Physical Surface(1) = {{surf{0}}}; \n".format(2))

# close files
f1.close()
