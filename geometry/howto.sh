#!/bin/sh

python geometry.py 
gmsh -2 membrane-potential.geo 
dolfin-convert membrane-potential.msh membrane-potential.xml
