# creator: duytruong
# date: 22.11.2017

from dolfin import *
#from mshr import *
#from context import model
#from model import geomcreator
#from model import meshhelper
import numpy as np


def exportToPNG(aFunction, minmax=[], name="aFunction", title="a function", elevateAngle=0):
    plotter = VTKPlotter(aFunction)
    if len(minmax) > 1:
        plotter.set_min_max(minmax[0], minmax[1])
    plotter.parameters["window_width"] = 1200
    plotter.parameters["window_height"] = 800
    plotter.parameters["title"] = title
    plotter.zoom(1.2)
    plotter.elevate(elevateAngle)

    plotter.parameters["mode"] = "color"
    plotter.plot()
    plotter.write_png("./results/"+name)

# parameters["allow_extrapolation"] = True
# PARAMETERS
width = 10*1E-6
height = 10*1E-6
radius = 3*1E-6

epsrCyt = 50 # [1] relative permitivity cytoplasm
epsrMed = 80 # [1] membrane
epsrMem = 9.0 # [1] medium

sigmaCyt = 0.53 # [S/m] cytoplasm
sigmaMem = 5*1E-7 # [S/m] membrane
sigmaMed = 1.2 # [S/m] medium

memThickness = 5*1E-9 # [m] membrane thickness

VUpper = 0.5*1000*height # E = 1V/mm = 1000V/m
VLower = -0.5*1000*height #

# MESHES
mesh = Mesh('./geometry/membrane-potential.xml')
subdomains = MeshFunction("size_t", mesh, "./geometry/membrane-potential_physical_region.xml")
boundaries = MeshFunction("size_t", mesh, "./geometry/membrane-potential_facet_region.xml")

meshOutside = SubMesh(mesh, subdomains, 0)
meshInside = SubMesh(mesh, subdomains, 1)

# plot(meshInside, "Mesh", interactive=True)

# FUNCTION SPACES
VOutside = FunctionSpace(meshOutside, "CG", 1) # medium == external domain
VInside = FunctionSpace(meshInside, "CG", 1) # cell == internal domain

W = VectorFunctionSpace(mesh, "DG", 0)
WOutside = VectorFunctionSpace(meshOutside, "DG", 0)
WInside = VectorFunctionSpace(meshInside, "DG", 0)

# Define test and trial functions
uOutside = TrialFunction(VOutside)
vOutside = TestFunction(VOutside)

uInside = TrialFunction(VInside)
vInside = TestFunction(VInside)

# boundaries
# left = CompiledSubDomain("near(x[0], side) && on_boundary", side = -0.5*width)
# right = CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.5*width)
circOutside = CompiledSubDomain("(x[0] < 0.48*width && x[0] > -0.48*width && x[1] < 0.48*height && x[1] > -0.48*height) && on_boundary", width=width, height=height)
circInside = CompiledSubDomain("on_boundary")

# WEAK FORMS
fOutside = Constant(0)
fInside = Constant(0)

# for connecting inside and outside domain
inbound = FacetFunction('size_t', meshOutside, 0)
inbound.set_all(0)
circOutside.mark(inbound, 1)
# plot(inbound, "Mesh", interactive=True)
dsInsideOutside = Measure('ds', domain=meshOutside, subdomain_data=inbound)
normalOutside = FacetNormal(meshOutside)

upper = CompiledSubDomain("near(x[1], side) && on_boundary", side = 0.5*height)
lower = CompiledSubDomain("near(x[1], side) && on_boundary", side = -0.5*height)
# upper.mark(inbound, 2)
# lower.mark(inbound, 3)
# plot(inbound, "Mesh", interactive=True)

EOutside = Function(WOutside) # electric field on and outside of membrane
solInside = Function(VInside) # potential in the cell
solOutside = Function(VOutside) # potential in the medium
u2OnBoundary = Function(VInside)

# solInside.interpolate(Constant(0))

set_log_level(WARNING)
iteration = 0
while iteration < 50:
    iteration += 1
    print "Iter " + str(iteration) + ":"

    # solve medium domain
    EInside = project(grad(solInside), WInside)
    EInside.set_allow_extrapolation(True)
    # EOutside.interpolate(EInside)
    # plot(project(EInsideProjected, WOutside))

    aOutside = inner(grad(uOutside), grad(vOutside))*dx
    LOutside = fOutside*vOutside*dx + dot(EInside, normalOutside)*vOutside*dsInsideOutside(1)
    bc1a = DirichletBC(VOutside, Constant(VUpper), upper)
    bc1b = DirichletBC(VOutside, Constant(VLower), lower)
    problemOutside = LinearVariationalProblem(aOutside, LOutside, solOutside, [bc1a, bc1b])
    solverOutside = LinearVariationalSolver(problemOutside)

    prm = solverOutside.parameters.krylov_solver  # short form
    prm.absolute_tolerance = 1E-12
    prm.relative_tolerance = 1E-7

    solverOutside.solve()

    # solve cell domain
    aInside = inner(grad(uInside), grad(vInside))*dx
    LInside = fInside*vInside*dx

    solOutside.set_allow_extrapolation(True)

    # u2OnBoundary.interpolate(solOutside)
    bc2a = DirichletBC(VInside, solOutside, circInside)
    problemInside = LinearVariationalProblem(aInside, LInside, solInside, [bc2a])
    solverInside = LinearVariationalSolver(problemInside)
    prm = solverInside.parameters.krylov_solver  # short form
    prm.absolute_tolerance = 1E-16
    prm.relative_tolerance = 1E-7

    solverInside.solve()

    EOutside = project(grad(solOutside), WOutside)

    if (iteration%1 == 0):
        exportToPNG(solOutside, name="VOutside{0}".format(iteration))
        exportToPNG(EOutside, name="EOutside{0}".format(iteration))
        #exportToPNG(u2OnBoundary, name="u2Circ{0}".format(iteration))
        exportToPNG(EInside, name="EInside{0}".format(iteration))
        exportToPNG(solInside, name="VInside{0}".format(iteration))

#plot(project(EInsideProjected, WOutside))
#plot(project(solOutsideProjected, VInside), mode="color")
plot(solOutside, mode="color")
plot(solInside, mode="color")
plot(EOutside)
plot(EInside, interactive=True)
